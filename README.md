# Transformer notebooks
Here are the notebooks for transformer-based approaches.
- `bert_finetuning_new_data.ipynb` has the code to preprocess data from the data engineering team and to train and evaluate BERT classifiers on it, both for coarse- and fine-label predictions.
- `eval_ensemble.ipynb` is for evaluation of the ensemble method. 
- `classification_on_embeddings.ipynb` has the code for embedding-based approaches with SentenceTransformers and OpenAI API.